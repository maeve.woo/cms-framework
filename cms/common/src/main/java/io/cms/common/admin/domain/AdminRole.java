package io.cms.common.admin.domain;

import org.springframework.security.core.GrantedAuthority;

public enum AdminRole implements GrantedAuthority {
    READ_ONLY("읽기 전용"),
    READ_WRITE("읽기 쓰기"),
    SUPER_ADMIN("어드민 관리자");

    private String description;

    AdminRole(String description) {
        this.description = description;
    }

    @Override
    public String getAuthority() {
        return null;
    }
}
