package io.cms.common.exception;

import lombok.Getter;

@Getter
public class CmsException extends RuntimeException {
    private Integer errorCode;
    private String messageKey = null;

    public CmsException(Integer errorCode, String messageKey) {
        super(messageKey);
        this.errorCode = errorCode;
        this.messageKey = messageKey;
    }

    public CmsException(Integer errorCode) {
        this.errorCode = errorCode;
    }
}
