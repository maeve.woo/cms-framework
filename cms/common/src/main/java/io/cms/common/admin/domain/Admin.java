package io.cms.common.admin.domain;

import io.cms.common.support.jpa.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Entity
@AllArgsConstructor
public class Admin extends BaseEntity {
    /**
     * 어드민 로그인 email
     */
    @Column(nullable = false)
    private String email;

    /**
     * 어드민 패스워드
     */
    @Column(nullable = false)
    private String password;

    /**
     * 최근 업데이트 시간
     */
    @Column(nullable = false)
    private LocalDateTime lastUpdated;

    @ElementCollection(targetClass = AdminRole.class)
    @Enumerated(EnumType.STRING)
    @Column(length = 30)
    private List<AdminRole> roles = new ArrayList<>();
    protected Admin() {}

    public Admin(String email, String password) {
        this.email = email;
        this.password = password;
        this.roles.add(AdminRole.READ_ONLY);
    }
}
