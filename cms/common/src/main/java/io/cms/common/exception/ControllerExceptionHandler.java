package io.cms.common.exception;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@ControllerAdvice
@RequiredArgsConstructor
public class ControllerExceptionHandler {
    private final MessageSource messageSource;

    @ExceptionHandler(Exception.class)
    public String exception(Exception e) throws Exception {
        log.error(e.getMessage());
        throw e;
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(CmsException.class)
    @ResponseBody
    public ErrorRes csmServerError(CmsException e) {
        log.error("CMS Ex: {}", e.getMessage());

        String messageKey = e.getMessageKey();
        if (messageKey != null) {
            messageSource.getMessage(messageKey, null, Locale.getDefault());
        } else {
            if (e.getMessage() != null) {
                messageKey = e.getMessage();
            } else {
                messageKey = "no error message";
            }
        }
        return new ErrorRes(e.getErrorCode(), messageKey);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public List<ErrorMsg> processValidationError(MethodArgumentNotValidException ex) {
        Stream<FieldError> errors = ex.getBindingResult().getFieldErrors().stream().filter(e -> e.getDefaultMessage() == null);
        if (errors.findAny().isEmpty()) {
            return errors.map(
                    e -> new ErrorMsg(
                            e.getField(), messageSource.getMessage(Objects.requireNonNull(e.getCode()), null, LocaleContextHolder.getLocale())
                    )
            ).collect(Collectors.toList());
        } else {
            return errors.map(
                    e -> new ErrorMsg(
                            e.getField(), e.getDefaultMessage()
                    )
            ).collect(Collectors.toList());
        }
    }

    @Data
    class ErrorRes {
        private Integer errorCode;
        private String messageKey;

        public ErrorRes(Integer errorCode, String messageKey) {
            this.errorCode = errorCode;
            this.messageKey = messageKey;
        }
    }

    @Data
    class ErrorMsg {
        private String field;
        private String message;

        public ErrorMsg(String field, String message) {
            this.field = field;
            this.message = message;
        }
    }
}
