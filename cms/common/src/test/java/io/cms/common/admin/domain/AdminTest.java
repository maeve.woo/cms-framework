package io.cms.common.admin.domain;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class AdminTest {

    @Autowired
    PasswordEncoder passwordEncoder;

    @DisplayName("기본 어드민 생성시 READ ONLY 권한을 갖음.")
    @Test
    public void createAdmin() {
        String test = "test";
        Admin admin = new Admin(test, test);

        assertThat(admin).isNotNull();
        assertThat(admin.getEmail()).isEqualTo(test);
        assertThat(admin.getRoles()).containsOnly(AdminRole.READ_ONLY);
    }
}