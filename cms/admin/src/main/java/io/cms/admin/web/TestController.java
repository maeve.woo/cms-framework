package io.cms.admin.web;

import io.cms.common.exception.CmsException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {

    @Value("${admin.error-code.unknown}")
    private Integer UNKNOWN;

    @GetMapping("/unknown-ex")
    public void unknownException() throws Exception {
        throw new Exception();
    }

    @GetMapping("/server-ex")
    public void serverException() {
        throw new CmsException(UNKNOWN);
    }
}
