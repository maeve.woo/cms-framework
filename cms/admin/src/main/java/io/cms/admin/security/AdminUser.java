package io.cms.admin.security;

import io.cms.common.admin.domain.Admin;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.io.Serializable;
import java.util.Collection;

public class AdminUser extends User implements Serializable {
    private Long id;
    private String email;

    public AdminUser(String username, String password, Collection<? extends GrantedAuthority> authorities, Long id, String email) {
        super(username, password, authorities);
        this.id = id;
        this.email = email;
    }

    public AdminUser(Admin admin) {
        this(admin.getEmail(), admin.getPassword(), admin.getRoles(), admin.getId(), admin.getEmail());
    }
}
