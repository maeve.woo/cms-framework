package io.cms.admin.security;

import io.cms.common.admin.domain.Admin;
import io.cms.common.admin.domain.AdminRepository;
import io.cms.common.admin.domain.AdminRole;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
public class AdminUserSecurityService implements UserDetailsService {
    private final PasswordEncoder passwordEncoder;
    private final AdminRepository adminRepository;

    @Value("${admin.init.id}")
    private String id;

    @Value("${admin.init.password}")
    private String password;


    @PostConstruct
    public void setRootUser() {
        List<Admin> admins = adminRepository.findAll();
        if(admins.isEmpty()) {
            Admin initAdmin = new Admin(id, passwordEncoder.encode(password), LocalDateTime.now(), List.of(AdminRole.SUPER_ADMIN));
            adminRepository.save(initAdmin);
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Admin> admin = adminRepository.findByEmail(username);

        if (admin.isPresent()) {
            return new AdminUser(admin.get());
        } else {
            throw new UsernameNotFoundException("{username} not found");
        }
    }
}
