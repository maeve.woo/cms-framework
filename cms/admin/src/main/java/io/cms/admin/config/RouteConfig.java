package io.cms.admin.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class RouteConfig implements WebMvcConfigurer {
    public void defaultViewRoute(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
    }
}
