package io.cms.admin.web;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ControllerExceptionTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void unknown() throws Exception {
        assertThatThrownBy(() -> mockMvc.perform(get("/test/unknown-ex")))
                .isInstanceOf(Exception.class);
    }

    @Test
    public void serverExcept() throws Exception {
        mockMvc.perform(get("/test/server-ex"))
                .andExpect(status().isInternalServerError());
    }
}